package com.mubaloo.friendface.data.api;

import com.mubaloo.friendface.data.models.Friend;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Adam Mayer on 29/09/2015.
 */
public interface FriendFaceService {

    @GET("/posts")
    void getFriendsPhotos(@Query("recipient") String friend, Callback<ArrayList<Friend>> cb);

}
