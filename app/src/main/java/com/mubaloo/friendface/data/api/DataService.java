package com.mubaloo.friendface.data.api;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by Adam Mayer on 29/09/2015.
 */
public class DataService {

    public static <S> S createService(Class<S> serviceClass) {

        OkHttpClient client = new OkHttpClient();
        Gson gson = new Gson();


        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setClient(new OkClient(client))
                .setEndpoint("Http://selfie-server.containers.aws.mub.lu")
                .setConverter(new GsonConverter(gson));

        RestAdapter adapter = builder.build();

        return adapter.create(serviceClass);
    }

}
