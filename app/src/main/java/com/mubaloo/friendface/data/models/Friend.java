package com.mubaloo.friendface.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Adam Mayer on 29/09/2015.
 */
public class Friend implements Parcelable {

    @SerializedName("imageUrl")
    private String imageURL;

    @SerializedName("recipient")
    private String recipient;

    @SerializedName("sender")
    private String sender;

    @SerializedName("_id")
    private String id;

    private Friend(Builder builder) {
        this.imageURL = builder.imageURL;
        this.recipient = builder.recipient;
        this.id = builder.id;
        this.sender = builder.sender;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getSender() {
        return sender;
    }

    public String getId() {
        return id;
    }


    public static class Builder {
        private String imageURL;
        private String recipient;
        private String id;
        private String sender;

        public Builder imageURL(String imageURL) {
            this.imageURL = imageURL;
            return this;
        }

        public Builder recipient(String recipient) {
            this.recipient = recipient;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder sender(String sender) {
            this.sender = sender;
            return this;
        }


        public Friend build() {
            return new Friend(this);
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imageURL);
        dest.writeString(this.recipient);
        dest.writeString(this.sender);
        dest.writeString(this.id);
    }

    protected Friend(Parcel in) {
        this.imageURL = in.readString();
        this.recipient = in.readString();
        this.sender = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<Friend> CREATOR = new Parcelable.Creator<Friend>() {
        public Friend createFromParcel(Parcel source) {
            return new Friend(source);
        }

        public Friend[] newArray(int size) {
            return new Friend[size];
        }
    };
}
