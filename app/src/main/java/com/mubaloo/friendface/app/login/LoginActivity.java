package com.mubaloo.friendface.app.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Spinner;

import com.mubaloo.friendface.R;
import com.mubaloo.friendface.app.Friends.FriendsActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    //================================================================================
    // VIEWS
    //================================================================================

    @Bind(R.id.login_logo)
    ImageView mLogo;

    @Bind(R.id.login_users)
    Spinner mUsers;

    //================================================================================
    // LIFECYCLE
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initialiseForm();
    }

    //================================================================================
    // UI METHODS
    //================================================================================

    private void initialiseForm() {
        mLogo.animate().setInterpolator(new LinearInterpolator()).scaleX(1.5F).scaleY(1.5F).rotation(360).setStartDelay(500).setDuration(1000).start();
    }

    //================================================================================
    // EVENTS
    //================================================================================

    @OnClick(R.id.login_login)
    public void showFriends() {
        FriendsActivity.start(this, mUsers.getSelectedItem().toString());
    }

}
