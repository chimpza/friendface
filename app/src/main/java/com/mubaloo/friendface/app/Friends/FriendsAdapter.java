package com.mubaloo.friendface.app.Friends;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mubaloo.friendface.R;
import com.mubaloo.friendface.app.BaseRecyclerAdapter;
import com.mubaloo.friendface.data.models.Friend;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Adam Mayer on 29/09/2015.
 */
public class FriendsAdapter extends BaseRecyclerAdapter<Friend, FriendsAdapter.ViewHolder> {


    public static final String PICTURE_URL_BASE = "http://selfie-server.containers.aws.mub.lu/";

    //================================================================================
    // MEMBER VARIABLES
    //================================================================================

    private LayoutInflater mInflater;
    protected Context mContext;
    private OnClickListener<Friend> mListener;

    //================================================================================
    // RECYCLER METHODS
    //================================================================================

    public FriendsAdapter(Context context, OnClickListener<Friend> onClickListener) {
        mContext = context;
        mListener = onClickListener;
        mInflater = LayoutInflater.from(context);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        OnFaceClickListener mListener;

        @Bind(R.id.friend_name)
        TextView mName;
        @Bind(R.id.friend_face)
        ImageView mFace;
        @Bind(R.id.item_container)
        View mContainer;


        public ViewHolder(View itemView, OnFaceClickListener onFaceClickListener) {

            super(itemView);

            mListener = onFaceClickListener;
            ButterKnife.bind(this, itemView);

            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(getAdapterPosition());
        }

        public interface OnFaceClickListener {
            void onClick(int position);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_friend, parent, false);

        return new ViewHolder(v, new ViewHolder.OnFaceClickListener() {
            @Override
            public void onClick(int position) {
                if (mListener != null) {
                    mListener.onClick(getItem(position), v);
                }
            }
        });
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Friend friend = getItem(position);
        holder.mName.setText(friend.getSender());
        Picasso.with(mContext)
                .load(PICTURE_URL_BASE + friend.getImageURL())
                .error(mContext.getDrawable(R.drawable.logo))
                .fit().centerCrop()
                .rotate(90)
                .into(holder.mFace);

        setAnimation(holder.mContainer, position);
        Log.d("adapter", "loading:" + PICTURE_URL_BASE + friend.getImageURL());
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.mContainer.clearAnimation();
    }

}
