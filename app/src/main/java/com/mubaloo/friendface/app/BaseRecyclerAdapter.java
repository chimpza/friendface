package com.mubaloo.friendface.app;

/**
 * Created by Adam Mayer on 29/09/2015.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.mubaloo.friendface.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class BaseRecyclerAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private List<T> mItems = new ArrayList<>();
    protected int mLastPosition;
    protected Context mContext;

    public void add(T t) {
        mItems.add(t);
        notifyDataSetChanged();
    }

    public void addAll(Collection<? extends T> collection) {
        if (collection != null) {
            mItems.addAll(collection);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }

    public void remove(T t) {
        mItems.remove(t);
        notifyDataSetChanged();
    }

    public void sortItems(Comparator<? super T> comparator) {
        Collections.sort(mItems, comparator);
        notifyDataSetChanged();
    }


    public T getItem(int position) {
        return mItems != null && mItems.size() > 0 ? mItems.get(position) : null;
    }

    public List<T> getItems() {
        return mItems;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public interface OnClickListener<T> {
        void onClick(T t, View view);
    }

    //-----------------------------------------------------
    // ANIMATION METHODS
    //-----------------------------------------------------

    protected void setAnimation(View viewToAnimate, int position) {
        if (mContext != null) {
            if (position > mLastPosition) {
                Animation animation;
                animation = AnimationUtils.loadAnimation(mContext, R.anim.abc_slide_in_bottom);
                viewToAnimate.startAnimation(animation);
                mLastPosition = position;
            }
        }
    }

}