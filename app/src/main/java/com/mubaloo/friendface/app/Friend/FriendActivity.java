package com.mubaloo.friendface.app.Friend;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.mubaloo.friendface.R;
import com.mubaloo.friendface.app.Friends.FriendsAdapter;
import com.mubaloo.friendface.data.models.Friend;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Adam Mayer on 29/09/2015.
 */
public class FriendActivity extends AppCompatActivity {
    //================================================================================
    // CONSTANTS
    //================================================================================

    private static final String ARG_USER = "user";

    //================================================================================
    // MEMBER VARIABLES
    //================================================================================

    private Friend mFriend;

    //================================================================================
    // VIEWS
    //================================================================================

    @Bind(R.id.friend_face)
    ImageView mFriendFace;

    @Bind(R.id.friend_name)
    TextView mFriendName;


    //================================================================================
    // LIFECYCLE
    //================================================================================

    public static void start(Activity activity, Friend friend, ImageView friendFace) {
        Intent intent = new Intent(activity, FriendActivity.class);
        intent.putExtra(ARG_USER, friend);
        ActivityCompat.startActivity(activity, intent, ActivityOptionsCompat.makeSceneTransitionAnimation(activity, friendFace, "robot").toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_content);
        postponeEnterTransition();
        ButterKnife.bind(this);
        mFriend = getIntent().getParcelableExtra(ARG_USER);
        loadImage();
    }

    private void loadImage() {
        Log.d("LoadThumbnail", "loading:" + FriendsAdapter.PICTURE_URL_BASE + mFriend.getImageURL());
        Picasso.with(this)
                .load(FriendsAdapter.PICTURE_URL_BASE + mFriend.getImageURL())
                .noFade()
                .rotate(90)
                .into(mFriendFace, mPicassoCallback);

        mFriendName.setText(mFriend.getSender());
        mFriendName.animate().setInterpolator(new BounceInterpolator()).scaleX(1.8f).scaleY(1.8F).setStartDelay(500).setDuration(1300).start();
    }

    Callback mPicassoCallback = new Callback() {
        @Override
        public void onSuccess() {
            startPostponedEnterTransition();
        }

        @Override
        public void onError() {

        }
    };


}
