package com.mubaloo.friendface.app.Friends;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.mubaloo.friendface.R;
import com.mubaloo.friendface.app.Friend.FriendActivity;
import com.mubaloo.friendface.data.api.DataService;
import com.mubaloo.friendface.data.api.FriendFaceService;
import com.mubaloo.friendface.data.models.Friend;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FriendsActivity extends AppCompatActivity implements FriendsAdapter.OnClickListener {

    //================================================================================
    // MEMBER VARIABLES
    //================================================================================

    private FriendFaceService mFriendFaceService;
    private ArrayList<Friend> mFriends;
    private String mFriendName;
    private FriendsAdapter mFriendsAdapter;

    //================================================================================
    // CONSTANTS
    //================================================================================

    private static final String ARG_USERID = "user_id";
    public static final String PARAM_FRIEND_LIST = "friends";

    //================================================================================
    // VIEWS
    //================================================================================

    @Bind(R.id.friends_list)
    RecyclerView mFriendsList;

    //================================================================================
    // LIFECYCLE
    //================================================================================

    public static void start(Context context, String userName) {
        Intent intent = new Intent(context, FriendsActivity.class);
        intent.putExtra(ARG_USERID, userName);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        ButterKnife.bind(this);

        initMenuBar();

        initIntentData();

        initInstanceData();

        initDataService();

        getData();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(PARAM_FRIEND_LIST, mFriends);
    }

    //================================================================================
    // INIT METHODS
    //================================================================================

    private void initDataService() {
        mFriendFaceService = DataService.createService(FriendFaceService.class);
    }

    private void initIntentData() {
        mFriendName = getIntent().getStringExtra(ARG_USERID);
    }

    private void initInstanceData() {
        mFriends = getIntent().getExtras().getParcelableArrayList(PARAM_FRIEND_LIST);
    }


    //================================================================================
    // MENU BAR
    //================================================================================

    private void initMenuBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Friends");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //================================================================================
    // UI METHODS
    //================================================================================

    private void populateList() {
        mFriendsList.setHasFixedSize(true);
        setRecyclerViewLayoutManager(mFriendsList);
        mFriendsAdapter = new FriendsAdapter(this, this);
        mFriendsList.setAdapter(mFriendsAdapter);
        mFriendsAdapter.addAll(mFriends);
        mFriendsAdapter.notifyDataSetChanged();
    }

    /**
     * Set RecyclerView's LayoutManager
     */
    public void setRecyclerViewLayoutManager(RecyclerView recyclerView) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (recyclerView.getLayoutManager() != null) {
            scrollPosition =
                    ((GridLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        }

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.scrollToPosition(scrollPosition);
    }

    //================================================================================
    // DATA METHODS
    //================================================================================

    private void getData() {
        if (mFriends == null) {
            Callback callback = new Callback() {
                @Override
                public void success(Object o, Response response) {
                    mFriends = (ArrayList<Friend>) o;
                    populateList();
                    Log.d("getData", "success:" + mFriends.size());

                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("getData", "Error:" + error.getMessage());
                }
            };

            mFriendFaceService.getFriendsPhotos(mFriendName, callback);
        } else {
            populateList();
        }

    }


    //================================================================================
    // EVENTS
    //================================================================================
    @Override
    public void onClick(Object friend, View view) {
        Log.d("FriendsActivity", "onclick:" + ((Friend) friend).getSender() );
        ImageView friendFace = (ImageView)view.findViewById(R.id.friend_face);
        Log.d("FriendsActivity", "onclick:" + friendFace.getTransitionName());
        FriendActivity.start(this,(Friend) friend, friendFace);
    }

}
